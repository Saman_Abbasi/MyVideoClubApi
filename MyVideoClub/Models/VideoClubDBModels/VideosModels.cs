﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyVideoClub.Models.VideoClubDBModels
{
    public class VideosModels
    {
        public string MovieName { get; set; }
        public string Topic { get; set; }
        public int MovieCount { get; set; }
        public string Description { get; set; }
        
    }
}