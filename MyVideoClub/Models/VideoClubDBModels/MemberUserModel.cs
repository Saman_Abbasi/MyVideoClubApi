﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyVideoClub.Models.VideoClubDBModels
{
    public class MemberUserModel
    {
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public int Age { get; set; }
        public DateTime RegisterDate { get; set; }
        public DateTime LastLoginDate { get; set; }
     
    }
}