﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using MyVideoClub.Api.Models.Database;
using System.Net.Http.Formatting;
using MyVideoClub.Api.Models.MemberUserModels;


namespace MyVideoClub.Api.Controllers
{
    public class MemberUserController : ApiController
    {
        VideoClubDBEntities1 db = new VideoClubDBEntities1();
        [HttpGet]
        public async Task<HttpResponseMessage> Get()
        {
            var model = db.MemberUser.ToArray();
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new ObjectContent<MemberUser[]>(model, new JsonMediaTypeFormatter())
            };
        }
        [HttpGet]
        public async Task<HttpResponseMessage> Get(Guid Id)
        {
            var model = db.MemberUser.FirstOrDefault(a => a.MemberUserId == Id);
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new ObjectContent<MemberUser>(model, new JsonMediaTypeFormatter())
            };
        }
        [HttpPost]
        public async Task<HttpResponseMessage> Post([FromBody] MemberUserModel value)
        {
            var result = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.Forbidden
            };
            if (string.IsNullOrEmpty(value.Name) || string.IsNullOrEmpty(value.Email))
            {
                return result;
            }

            var model = new MemberUser()
            {
                RegisterDate = DateTime.Now.Date,
                PasswordHash = value.PasswordHash,
                MemberUserId = Guid.NewGuid(),
                Name = value.Name,
                Lastname = value.Lastname,
                Age = value.Age,
                Email = value.Email,
            };
            db.MemberUser.Add(model);
            db.SaveChanges();
            result.StatusCode = HttpStatusCode.Created;
            return result;
        }
        [HttpPut]
        public async Task<HttpResponseMessage> Put(Guid id, [FromBody] MemberUserModel value)
        {
            var model = db.MemberUser.FirstOrDefault(a => a.MemberUserId == id);
            model.Name = value.Name;
            model.Lastname = value.Lastname;
            model.Email = value.Email;
            model.Age = value.Age;
            model.PasswordHash = value.PasswordHash;
            db.SaveChanges();
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
            };
        }
        [HttpDelete]
        public async Task<HttpResponseMessage> Delete(Guid id)
        {
            var model = db.MemberUser.FirstOrDefault(a => a.MemberUserId == id);
            db.MemberUser.Remove(model);
            db.SaveChanges();
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK
            };
        }
    }
}
