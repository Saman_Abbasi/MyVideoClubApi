﻿using MyVideoClub.Api.Models.Database;
using MyVideoClub.Api.Models.MemberUserModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MyVideoClub.Api.Controllers
{
    public class RentController : ApiController
    {
        VideoClubDBEntities1 db = new VideoClubDBEntities1();
        [HttpGet]
        public async Task<HttpResponseMessage> Get()
        {
            var movieModel = new Movie();
            var memberModel = new MemberUser();
            var model = new Rent()
            {
                Name = memberModel.Name,
                Lastname = memberModel.Lastname,
                Email = memberModel.Email,
                MemberUserId = memberModel.MemberUserId,
                MovieId = movieModel.MovieId,
                MovieName = movieModel.MovieName,
            };
            return null;
        }
        [HttpPost]
        public async Task<HttpResponseMessage> Post(Rent model)
        {
            var member = db.MemberUser.FirstOrDefault(s => s.MemberUserId == model.MemberUserId);
            var movie = db.Movie.FirstOrDefault(s => s.MovieId == model.MovieId);
            member.Movie.Add(movie);
            try
            {
                db.SaveChanges();
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Created
                    //Content = new StringContent("با موفقیت ثبت شد ")
                };
            }
            catch (Exception)
            {

                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Conflict
                };
            }
           
        }
    }
}
