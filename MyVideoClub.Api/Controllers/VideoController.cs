﻿using MyVideoClub.Api.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;

namespace MyVideoClub.Api.Controllers
{
    public class VideoController : ApiController
    {
        VideoClubDBEntities1 db = new VideoClubDBEntities1();
        [HttpGet]
        public async Task<HttpResponseMessage> Get()
        {
            var model = db.Movie.ToArray();
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new ObjectContent<Movie[]>(model, new JsonMediaTypeFormatter())
                //inja be model db cast konim ya class khodemon ????
            };
        }
        [HttpGet]
        public async Task<HttpResponseMessage> Get(Guid id)
        {
            var model = db.Movie.FirstOrDefault(a => a.MovieId == id);
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new ObjectContent<Movie>(model, new JsonMediaTypeFormatter())
            };
        }
        [HttpPost]
        public async Task<HttpResponseMessage> Post([FromBody] Movie value)
        {
            var result = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.Forbidden
            };
            if (string.IsNullOrEmpty(value.MovieName))
            {
                return result;
            }
            var model = new Movie()
            {
                MovieName = value.MovieName,
                MovieCount = value.MovieCount,
                Topic = value.Topic,
                Description = value.Description,
                MovieId = Guid.NewGuid()
            };
            db.Movie.Add(model);
            db.SaveChanges();
            result.StatusCode = HttpStatusCode.Created;
            return result;
        }
        [HttpPut]
        public async Task<HttpResponseMessage> Put([FromBody] Movie value, Guid id)
        {
            var result = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.Forbidden
            };
            //if (string.IsNullOrEmpty(value.MovieName))
            //{
            //    return result;
            //}
            var model = db.Movie.FirstOrDefault(a => a.MovieId == id);
            model.MovieName = value.MovieName;
            model.MovieCount = value.MovieCount;
            model.Description = value.Description;
            model.Topic = value.Topic;
            model.MovieId = value.MovieId;
            db.SaveChanges();
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
            };
        }
        [HttpDelete]
        public async Task<HttpResponseMessage> Delete(Guid id)
        {
            var model = db.Movie.FirstOrDefault(a => a.MovieId == id);
            db.Movie.Remove(model);
            db.SaveChanges();
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
            };
        }
    }
}
