﻿using MyVideoClub.Api.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;

namespace MyVideoClub.Api.Controllers
{
    public class LoginUserController : ApiController
    {
        VideoClubDBEntities1 db = new VideoClubDBEntities1();
        [HttpGet]
        public async Task<HttpResponseMessage> Get()
        {
            var model = db.MemberUser.ToArray();
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new ObjectContent<MemberUser[]>(model, new JsonMediaTypeFormatter())
            };
        }
        [HttpGet]
        public async Task<HttpResponseMessage> Get(string email)
        {
            var model = db.MemberUser.FirstOrDefault(a => a.Email == email);
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new ObjectContent<MemberUser>(model, new JsonMediaTypeFormatter())
            };
        }
        
    }
}
