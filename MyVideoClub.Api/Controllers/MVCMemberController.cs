﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using RestSharp;
using MyVideoClub.Api.Models.MemberUserModels;

namespace MyVideoClub.Api.Controllers
{
    public class MemberController : Controller
    {
        // GET: Member
        public ActionResult Index()
        {
            var getJson = SimpleGetAsync("/api/MemberUser");
            var model = JsonConvert.DeserializeObject<MemberUserModel[]>(getJson.Content);
            return View();
        }
        public IRestResponse SimpleGetAsync(string url)
        {
            var client = new RestClient(url);
            var restRq = new RestRequest()
            {
                Method = Method.GET,
                RootElement = "/",
                RequestFormat = DataFormat.Json
            };
            var restRs = client.Execute(restRq);
            return restRs;
        }
        protected static IRestResponse SimplePost<T>(object objectToUpdate, string url) where T : new()
        {

            var client = new RestClient(url);
            var restRq = new RestRequest()
            {
                Method = Method.POST,
                RootElement = "/",
                RequestFormat = DataFormat.Json
            };
            var json = JsonConvert.SerializeObject(objectToUpdate);
            restRq.AddHeader("Authorization", "Basic ");// + authKey);
            restRq.AddParameter("text/json", json, ParameterType.RequestBody);

            var response = client.Execute<T>(restRq);
            return response;
        }
    }
}
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//var client = new RestClient();
////var client = new RestClient("");


//var restRq = new RestRequest(url)
//{
//    Method = Method.POST,
//    RootElement = "/",
//    RequestFormat = DataFormat.Json
//};
//var json = JsonConvert.SerializeObject(objectToUpdate);
//restRq.AddHeader("Authorization", "Basic " + authKey);
//       restRq.AddParameter("text/json", json, ParameterType.RequestBody);

//        var response = client.Execute < T😠restRq);


//        return response;
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//    using RestSharp;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using Newtonsoft.Json;
//namespace WebApplication1.Controllers
//{
//    public class HomeController : Controller
//    {
//        public ActionResult Index()
//        {
//            var getJson = SimpleGetAsync("http://localhost:23431/api/saman");
//            var model = JsonConvert.DeserializeObject < Venue[]😠getJson.Content);
//            return View();
//        }
//        public IRestResponse SimpleGetAsync(string url)
//        {
//            var client = new RestClient(url);
//            var restRq = new RestRequest()
//            {
//                Method = Method.GET,
//                RootElement = "/",
//                RequestFormat = DataFormat.Json
//            };
//            var restRs = client.Execute(restRq);
//            return restRs;
//        }
//        protected static IRestResponse SimplePost<T😠object objectToUpdate, string url) where T : new()
//        {

//            var client = new RestClient(url);
//            //var client = new RestClient("");


//            var restRq = new RestRequest()
//            {
//                Method = Method.POST,
//                RootElement = "/",
//                RequestFormat = DataFormat.Json
//            };
//            var json = JsonConvert.SerializeObject(objectToUpdate);
//            var response = client.Execute < T😠restRq);
//            return response;
//        }
//       }
//}