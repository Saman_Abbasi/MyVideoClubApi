﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyVideoClub.Api.Models.MemberUserModels
{
    public class Video
    {
        public Guid MovieId { get; set; }
        public string MovieName { get; set; }
        public string Description { get; set; }
        public string Topic { get; set; }
        public int MovieCount { get; set; }
        
    }
}