﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyVideoClub.Api.Models.MemberUserModels
{
    public class LoginUser
    {
        public string PasswordHash { get; set; }
        public string Email { get; set; }

    }
}