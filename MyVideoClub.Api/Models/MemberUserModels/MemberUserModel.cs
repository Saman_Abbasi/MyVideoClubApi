﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyVideoClub.Api.Models.MemberUserModels
{
    public class MemberUserModel
    {
        public Guid MemberUserId { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public int Age { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }

    }
}